// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwbot::{Bot, Page};
use serde::Deserialize;

#[derive(Deserialize)]
struct Response {
    purge: Vec<PurgeResponse>,
}

#[derive(Deserialize)]
struct PurgeResponse {
    // ns: i32,
    // title: String,
    purged: bool,
}

async fn purge(bot: &Bot, page: Page) -> Result<()> {
    print!("Purging {}...", page.title());
    let resp: Response = bot
        .api()
        .post_with_token(
            "csrf",
            &[
                ("action", "purge"),
                // We could purge all the titles at once but this is just
                // a simple demo script
                ("titles", page.title()),
            ],
        )
        .await?;
    if resp.purge[0].purged {
        println!("done!");
    } else {
        println!("error :(");
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let bot = Bot::from_default_config().await?;
    for arg in std::env::args().skip(1) {
        let page = bot.page(&arg)?;
        purge(&bot, page).await?;
    }
    Ok(())
}
