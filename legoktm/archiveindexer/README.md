Archive indexer
===============

Generates archive indexes for talk page discussions

See <https://en.wikipedia.org/wiki/User:HBC_Archive_Indexerbot/OptIn> for details.

Named "hbcai" in honor of the original bot's name.
