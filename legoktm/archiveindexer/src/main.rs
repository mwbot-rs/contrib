/*
Copyright (C) 2012, 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use anyhow::{anyhow, Result};
use mwbot::parsoid::*;
use mwbot::{generators, Bot, Error, Page, SaveOptions};
use std::sync::Arc;

mod mask;
mod parser;
mod template;
mod thread;

use crate::mask::Mask;
use crate::template::Template;
type DefaultTemplate = Arc<Template>;

// Keep in sync with get_month()
const MONTH_REGEX: &str = "(January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)";

#[derive(Debug)]
struct Instructions {
    origin: String,
    target: String,
    masks: Vec<Mask>,
    template: String,
}

fn has_comment(code: ImmutableWikicode) -> bool {
    code.into_mutable().filter_comments().iter().any(|comment| {
        [
            "HBC Archive Indexerbot can blank this",
            "Legobot can blank this",
        ]
        .contains(&comment.text().trim())
    })
}

async fn follow_instructions(
    bot: &Bot,
    instructions: Instructions,
    default_template: DefaultTemplate,
) -> Result<()> {
    let target = bot.page(&instructions.target)?;
    let code = match target.html().await {
        Ok(code) => code,
        Err(Error::PageDoesNotExist(_)) => {
            return Err(anyhow!(
                "target [[{}]] does not exist.",
                target.title()
            ));
        }
        Err(err) => return Err(err.into()),
    };
    let redirect = {
        let code = code.clone().into_mutable();
        code.redirect().map(|redir| redir.target())
    };
    let (target_code, target) = if let Some(redirect) = redirect {
        let target = bot.page(&redirect)?;
        let code = target.html().await?;
        (code, target)
    } else {
        (code, target)
    };
    if !has_comment(target_code) {
        return Err(anyhow!(
            "target ([[{}]]) missing safe string",
            target.title()
        ));
    }
    // TODO: can we identify duplicates from overlapping masks?
    let mut threads = vec![];
    for mask in &instructions.masks {
        threads.extend(mask.expand(bot).await?);
    }
    threads.sort_by(|a, b| a.first.cmp(&b.first));
    if threads.is_empty() {
        return Err(anyhow!("found 0 threads, misconfiguration?",));
    }
    println!("building template for {}", target.title());
    let new_wikitext = if instructions.template
        == "User:HBC Archive Indexerbot/default template"
    {
        default_template.build(threads, &instructions)
    } else {
        match template::get_template(bot, &instructions.template).await {
            Ok(template) => template.build(threads, &instructions),
            Err(_) => {
                // TODO: log this error
                default_template.build(threads, &instructions)
            }
        }
    };
    let old_wikitext = target.wikitext().await?;
    if old_wikitext.trim() == new_wikitext.trim() {
        // No changes needed!
        println!("No changes needed for [[{}]]", target.title());
        return Ok(());
    }
    let title = target.title().to_string();
    target
        .save(new_wikitext, &SaveOptions::summary("Bot: Updating index"))
        .await?;
    println!("Saved [[{}]]", &title);
    Ok(())
}

async fn handle_page(
    bot: Bot,
    page: Page,
    default_template: DefaultTemplate,
) -> Result<()> {
    let instructions = parser::parse_instructions(&page).await?;
    dbg!(&instructions);
    follow_instructions(&bot, instructions, default_template).await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let bot = Bot::from_default_config().await.unwrap();
    let default_template = Arc::new(
        template::get_template(
            &bot,
            "User:HBC Archive Indexerbot/default template",
        )
        .await?,
    );
    /*handle_page(
        bot.clone(),
        bot.page("User talk:Legoktm")?,
        default_template.clone(),
    )
    .await
    .unwrap();*/

    let mut handles = vec![];
    let mut stream =
        generators::embeddedin(&bot, "User:HBC Archive Indexerbot/OptIn");
    while let Some(page) = stream.recv().await {
        let page = page?;
        let bot = bot.clone();
        let default_template = default_template.clone();
        handles.push(tokio::spawn(async move {
            let title = page.title().to_string();
            match handle_page(bot, page, default_template).await {
                Ok(ok) => Ok(ok),
                // Add context of the origin page to all errors
                Err(err) => Err(anyhow!("[[{}]]: {}", title, err)),
            }
        }));
    }
    let mut errors = vec![];
    for handle in handles {
        match handle.await {
            Ok(Ok(_)) => {}
            Ok(Err(err)) => errors.push(err.to_string()),
            Err(err) => println!("join error: {}", err),
        }
    }
    errors.sort();
    let errors = errors
        .iter()
        .map(|error| format!("* {}", error))
        .collect::<Vec<_>>()
        .join("\n");
    println!("{}", &errors);
    let page = bot.page("User:Legobot/Index errors")?;
    page.save(
        errors,
        &SaveOptions::summary("Bot: Updating list of errors"),
    )
    .await?;
    Ok(())
}
