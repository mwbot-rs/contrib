/**
Copyright (C) 2012, 2021-2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use anyhow::Result;
use mwapi_responses::prelude::*;
use mwbot::{parsoid::*, ApiClient, Bot, SaveOptions};
use tracing::{error, info};

#[derive(Debug)]
struct CategoryMember {
    talk_title: String,
    main_title: String,
}

#[query(
    prop = "info",
    inprop = "associatedpage",
    generator = "categorymembers",
    gcmlimit = "max"
)]
struct CategoryMemberResponse {}

/// Query the current protection status of a page
async fn category_members(
    name: &str,
    api: &ApiClient,
) -> Result<Vec<CategoryMember>> {
    let result: CategoryMemberResponse =
        api.query_response(&[("gcmtitle", name)]).await?;
    let status: Vec<_> = result
        .items()
        .map(|page| CategoryMember {
            talk_title: page.title.to_string(),
            main_title: page.associatedpage.to_string(),
        })
        .collect();
    Ok(status)
}

#[query(prop = "revisions", rvprop = "ids", rvlimit = "max")]
struct IdsResponse;

async fn get_revision_ids(name: &str, api: &ApiClient) -> Result<Vec<u64>> {
    let result: IdsResponse = api.query_response(&[("titles", name)]).await?;
    let revs: Vec<_> = result
        .items()
        .flat_map(|page| page.revisions.iter().map(|rev| rev.revid))
        .collect();
    Ok(revs)
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().init();
    let bot = Bot::from_default_config().await?;
    let members =
        category_members("Category:Good articles without an oldid", bot.api())
            .await?;
    let mut handles = vec![];
    for member in members {
        let bot = bot.clone();
        handles.push(tokio::spawn(async move {
            match handle_page(&member, &bot).await {
                Ok(_) => {}
                Err(err) => error!("{} failed: {:?}", &member.talk_title, err),
            };
        }));
    }
    for handle in handles {
        handle.await.unwrap();
    }

    info!("Finished successfully");
    Ok(())
}

async fn handle_page(member: &CategoryMember, bot: &Bot) -> Result<()> {
    info!("Looking up page={}", &member.main_title);

    let revisions = get_revision_ids(&member.main_title, bot.api()).await?;
    let mut ever_found = false;
    let mut previous: Option<u64> = None;
    let page = bot.page(&member.main_title)?;
    for revision in revisions {
        info!("{}: Looking at id={}", &member.main_title, &revision);
        let html = page.revision_html(revision).await?.into_mutable();
        let found = html
            .inclusive_descendants()
            .filter_map(|node| node.as_indicator())
            .any(|temp| temp.name().unwrap() == "good-star");
        if found {
            ever_found = true;
            previous = Some(revision);
            continue;
        } else if !ever_found {
            info!(
                "{}: Doesn't have Template:Good article, skipping",
                &member.main_title
            );
            return Ok(());
        }

        // Did not find the <indicator>
        if let Some(revid) = previous {
            // This is the one!
            info!("Template was added in id={}", revid);
            break;
        }
    }

    let oldid = match previous {
        Some(id) => id,
        None => {
            return Ok(());
        }
    };

    let talk_page = bot.page(&member.talk_title)?;
    let html = {
        let talk_html = talk_page.html().await?.into_mutable();
        let mut found_template = false;
        for template in talk_html.filter_templates()? {
            if template.name() == "Template:GA" {
                template.set_param("oldid", &oldid.to_string())?;
                found_template = true;
                break;
            }
        }

        if !found_template {
            info!("Unable to find Template:GA on page={}", &member.talk_title);
            return Ok(());
        }
        talk_html.into_immutable()
    };
    talk_page
        .save(
            html,
            &SaveOptions::summary(
                "Bot: Setting |oldid= for {{[[Template:GA|GA]]}}",
            ),
        )
        .await?;

    Ok(())
}
