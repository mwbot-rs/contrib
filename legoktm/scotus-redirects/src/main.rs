/**
Copyright (C) 2012, 2021-2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use anyhow::Result;
use mwbot::generators::categorymembers_recursive;
use mwbot::parsoid::prelude::*;
use mwbot::{Bot, Error, Page, SaveOptions};
use std::collections::HashSet;

async fn handle_page(bot: &Bot, page: Page) -> Result<bool> {
    let new = page.title().replace(" v. ", " v ");
    let newpage = bot.page(&new)?;
    if !should_create_redirect(&page, &newpage).await? {
        return Ok(false);
    }
    let target = page.title();
    let code = {
        let redirect = Redirect::new(target);
        let template = Template::new_simple("R from modification");
        let code = Wikicode::new("");
        code.append(&redirect);
        // For better wikitext spacing
        code.append(&Wikicode::new_text("\n\n"));
        code.append(&template);
        code.into_immutable()
    };
    println!("Redirecting [[{}]] to [[{}]]", &new, target);
    newpage
        .save(
            code,
            &SaveOptions::summary(
                "Bot: Creating redirect for alternate punctuation",
            ),
        )
        .await?;
    Ok(true)
}

async fn should_create_redirect(page: &Page, newpage: &Page) -> Result<bool> {
    // Should not create if it's a redirect
    if page.is_redirect().await? {
        println!("{} is redirect", page.title());
        return Ok(false);
    }

    // Create if it doesn't exist
    match newpage.html().await {
        Ok(_) => Ok(false),
        Err(Error::PageDoesNotExist(_)) => Ok(true),
        Err(err) => Err(err.into()),
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let bot = Bot::from_default_config().await?;
    let mut gen = categorymembers_recursive(
        &bot,
        "Category:United States Supreme Court cases",
    );
    let mut handles = vec![];
    // Keep track of page duplicates
    let mut seen: HashSet<String> = HashSet::new();
    while let Some(page) = gen.recv().await {
        let page = page?;
        if page.title().contains(" v. ") {
            if seen.contains(&page.title().to_string()) {
                continue;
            }
            seen.insert(page.title().to_string());
            let bot = bot.clone();
            handles.push(tokio::spawn(
                async move { handle_page(&bot, page).await },
            ));
        }
    }
    let mut count = 0;
    for handle in handles {
        if handle.await.unwrap().unwrap() {
            count += 1;
        }
    }
    println!("Created {} redirects.", count);
    Ok(())
}
