/**
Copyright (C) 2013, 2021, 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use anyhow::{anyhow, Result};
use mwapi_responses::{
    prelude::*, protection::ProtectionInfo, timestamp::Expiry,
};
use mwbot::{Bot, Error};
use serde_json::Value;
use time::format_description::FormatItem;
use time::macros::format_description;
use time::{Date, Duration, OffsetDateTime};
use tracing::info;

const TFA_FORMAT: &[FormatItem] =
    format_description!("[month repr:long] [day padding:none], [year]");
const MW_TIMESTAMP: &[FormatItem] =
    format_description!("[year]-[month]-[day]T[hour]:[minute]:[second]Z");

#[query(prop = "info", inprop = "protection")]
struct ProtectionResponse;

/// Query the current protection status of a page
async fn protection_status(
    name: &str,
    bot: &Bot,
) -> Result<Vec<ProtectionInfo>> {
    let resp: ProtectionResponse =
        bot.api().query_response(&[("titles", name)]).await?;
    Ok(resp.query.pages[0].protection.clone())
}

async fn handle_page(name: &str, day: Date, bot: &Bot) -> Result<()> {
    let status = protection_status(name, bot).await?;
    // dbg!(&status);
    // let title = Title::new_from_full(name, api);
    // Protect for a day
    let until = day + Duration::days(1);
    let mut protections = vec![];
    let mut expiries = vec![];
    let mut extending = false;

    for prot in status.iter() {
        if prot.type_ == "edit" {
            if prot.expiry.is_infinity() {
                // Protected forever, we're set.
                info!("{} is already protected", name);
                return Ok(());
            }

            let expiry = prot.expiry.as_timestamp().unwrap();
            if expiry.date() >= until {
                // Protected past Main Page date, we're set.
                info!("{} is already protected", name);
                return Ok(());
            }
            // It is protected, but not for long enough. We'll just extend the expiry
            // at the *same* level, regardless of whatever it is.
            info!("{} needs protection to be extended!", name);
            extending = true;
            protections.push(format!("edit={}", prot.level));
            expiries.push(
                until
                    .with_hms(0, 0, 0)
                    // unwrap: Safe because we hardcode 0s
                    .unwrap()
                    .format(&MW_TIMESTAMP)
                    .unwrap(),
            );
        }
    }
    if !extending {
        info!("{} needs protection!", name);
        protections.push("edit=autoconfirmed".to_string());
        expiries.push(
            until
                .with_hms(0, 0, 0)
                // unwrap: Safe because we hardcode 0s
                .unwrap()
                .format(&MW_TIMESTAMP)
                .unwrap(),
        );
    }
    let mut cascade = false;
    for prot in status.iter() {
        if prot.type_ == "aft" {
            // bug 57389
            continue;
        }
        if prot.source.is_some() {
            // skip cascading protection
            continue;
        }
        if prot.type_ == "edit" {
            // don't try to protect what we're changing
            continue;
        }
        if prot.cascade {
            // send it back i guess?
            cascade = true;
        }
        protections.push(format!("{}={}", prot.type_, prot.level));
        // TODO: upstream to mwapi_responses
        let formatted = match prot.expiry {
            Expiry::Infinity => "infinity".to_string(),
            Expiry::Finite(ts) => ts.format(&MW_TIMESTAMP).unwrap(),
        };
        expiries.push(formatted);
    }

    let protections = protections.join("|");
    let expiries = expiries.join("|");
    let mut params = vec![
        ("action", "protect"),
        ("title", name),
        ("protections", &protections),
        ("expiry", &expiries),
        ("reason", "Semi-protecting upcoming [[WP:TFA|TFA]] (30-day trial, [[Wikipedia:Bots/Requests for approval/TFA Protector Bot 3|provide feedback]])"),
    ];
    if cascade {
        params.push(("cascade", "1"));
    }
    info!("Protection options: {}", serde_json::to_string(&params)?);
    let _: Value = bot.api().post_with_token("csrf", &params).await?;
    info!("Successfully protected {}", &name);
    Ok(())
}

async fn get_tfa_title(day: Date, bot: &Bot) -> Result<String> {
    // First see if we can get it from Template:TFA title
    let tfa_page = bot.page(&format!(
        "Template:TFA title/{}",
        day.format(TFA_FORMAT).unwrap()
    ))?;
    match tfa_page.wikitext().await {
        Ok(text) => return Ok(text),
        // Nothing, keep trying
        Err(Error::PageDoesNotExist(_)) => {
            info!("{} didn't exist, will check TFA now", tfa_page.title())
        }
        Err(e) => {
            return Err(anyhow!(e.to_string()));
        }
    };

    // Try harder, parse it out of the TFA page itself
    extract_tfa_title(day, bot).await
}

async fn extract_tfa_title(day: Date, bot: &Bot) -> Result<String> {
    use mwbot::parsoid::prelude::*;

    let page = bot.page(&format!(
        "Wikipedia:Today's featured article/{}",
        day.format(TFA_FORMAT).unwrap()
    ))?;
    let code = page.html().await?.into_mutable();
    // Return the first bolded link
    for bold in code.select("b").iter() {
        // unwrap: We know that <b> tags turn into generic nodes
        let links = bold.as_generic().unwrap().filter_links();
        if !links.is_empty() {
            return Ok(links[0].target());
        }
    }
    Err(anyhow!("could not find title for {}", page.title()))
}

#[tokio::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "mwapi=debug,info");
    tracing_subscriber::fmt::init();
    let bot = Bot::from_default_config().await?;
    // Tomorrow
    let day = OffsetDateTime::now_utc().date() + Duration::days(1);
    let text = match get_tfa_title(day, &bot).await {
        Ok(text) => text,
        Err(e) => {
            info!("{}", e);
            info!("{} is missing, skipping", day.format(TFA_FORMAT).unwrap());
            return Ok(());
        }
    };
    let page = bot.page(&text)?;
    // Follow any redirects
    let page = match page.redirect_target().await? {
        Some(target) => {
            info!("{} redirects to {}", &text, &target.title());
            target
        }
        None => page,
    };
    handle_page(page.title(), day, &bot).await?;
    info!("Finished successfully!");
    Ok(())
}
